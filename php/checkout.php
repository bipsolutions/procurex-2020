<?php
include '../static/dist/vendor/autoload.php';
?>
<script src="https://js.stripe.com/v3/"></script>
<?php
// Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
\Stripe\Stripe::setApiKey('sk_live_ngik3vgZ1SC0b56iwTU83RTy00o1S4kUR8');

$session = \Stripe\Checkout\Session::create([
  'payment_method_types' => ['card'],
  'line_items' => [[
    'name' => 'Procurex 2020 Private Sector',
    'description' => 'Procurex 2020 Private Sector Event Ticket',
    'amount' => 9500,
    'currency' => 'gbp',
    'quantity' => $_GET['qty']
  ],
  [
    'name' => 'VAT',
    'description' => 'VAT',
    'amount' => 1900,
    'currency' => 'gbp',
    'quantity' => $_GET['qty']
  ]],
  'success_url' => 'https://procurexnational.co.uk/success',
  'cancel_url'  => 'https://procurexnational.co.uk/cancelled',
  'customer'    => $_GET['customer'],
]);

$id = $session['id'];
?>

<script>
var stripe = Stripe('pk_test_pzPEaQLnKjkQUh1krA2EWkLz00q4BvnEHL');

stripe.redirectToCheckout({
// Make the id field from the Checkout Session creation API response
// available to this file, so you can provide it as parameter here
// instead of the {{CHECKOUT_SESSION_ID}} placeholder.
sessionId: '<?php echo $id; ?>'
}).then(function (result) {
    alert(result.error.message)
});

</script>
