<?php
include '../static/dist/vendor/autoload.php';
require 'email.php';
// Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
\Stripe\Stripe::setApiKey('sk_live_ngik3vgZ1SC0b56iwTU83RTy00o1S4kUR8');

// You can find your endpoint's secret in your webhook settings
$endpoint_secret = 'whsec_JvQteiP5n17jFmw2iXHJkPTZ9zhNycqZ';

$payload = @file_get_contents('php://input');
$sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
$event = null;

try {
  $event = \Stripe\Webhook::constructEvent(
    $payload, $sig_header, $endpoint_secret
  );
} catch(\UnexpectedValueException $e) {
  // Invalid payload
  http_response_code(400);
  exit();
} catch(\Stripe\Error\SignatureVerification $e) {
  // Invalid signature
  http_response_code(400);
  exit();
}

// Handle the checkout.session.completed event
if ($event->type == 'checkout.session.completed') {
  $session = $event->data->object;
  // Fulfill the purchase...
  handle_checkout_session($session);
}

$type = $_GET['ticket_type'];

http_response_code(200);

function handle_checkout_session($session){
    send_mail('eventbookings@bipsolutions.com', 'Ticket Purchased', 'a ticket was purchased'.$session['id']);
    insert_ticket_db($session);
}

function insert_ticket_db($session){
    DB::insert('procurex_2020_tickets', array(
        'registrant_id' => $session['customer'],
        'type' => 'paid',
        'timestamp' => time()
    ));
}