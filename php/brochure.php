<?php
include '../static/dist/vendor/autoload.php';
require 'email.php';

$context['name'] = $_POST['brochureName'];
$context['email'] = $_POST['brochureEmail'];
$context['company'] = $_POST['brochureCompany'];
//Add in Telephone Number
$context['phone'] = $_POST['brochurePhone'];
//Add in Turnover
$context['turnover'] = $_POST['brochureTurnover'];
//Add in Op Sector
$context['opSector'] = $_POST['brochureOpSector'];
//Add in Supply to Public
$context['supplyToPublic'] = $_POST['brochureSupplyToPublic'];

// Check if all submitted fields are not empty. Return error if any of them are empty (bypassed js validation)
foreach($context as $key => $value){
    if(empty($value)){
        echo 'incomplete_form';
        die;
    }
}

$body = $twig->render('brochure-email.twig', $context);
$adminEmail = $twig->render('admin-brochure.twig', $context);

send_mail($_POST['brochureEmail'], 'Your Sales Brochure', $body);
send_mail('eventsteam@bipsolutions.com', 'Admin Notification Sales Brochure Submission', $adminEmail);
send_mail('gus.mackenzie@bipsolutions.com', 'Admin Notification Sales Brochure Submission', $adminEmail);
send_mail('karen.milligan@bipsolutions.com', 'Admin Notification Sales Brochure Submission', $adminEmail);

$data = $_POST;

DB::insert('sales_brochure_submissions', [
    //"contact_id" => $customer['id'],
    "brochureName"  =>  $_POST['brochureName'],
    "brochureEmail" => $_POST['brochureEmail'],
    "brochureCompany" => $_POST['brochureCompany'],
    //Add Telephone
    "brochurePhone" => $_POST['brochurePhone'],
    //Add Turnover
    "brochureTurnover" => $_POST['brochureTurnover'],
    //Add Op Sector
    "brochureOpSector" => $_POST['brochureOpSector'],
    //Add Supply To Public
    "brochureSupplyToPublic" => $_POST['brochureSupplyToPublic'],
    "timestamp" => new DateTime("now")
]);

//echo $customer['id'];
?>
