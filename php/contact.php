<?php
include '../static/dist/vendor/autoload.php';
require 'email.php';

$context['name'] = $_POST['contactFirstName'] . ' ' . $_POST['contactLastName'];
$context['company'] = $_POST['contactCompany'];
$context['email'] = $_POST['contactEmail'];
$context['enquiry'] = $_POST['contactEnquiry'];
$context['orgType'] = $_POST['contactOrgType'];
$context['message'] = $_POST['contactMessage'];

$body = $twig->render('contact-email.twig', $context);
$adminEmail = $twig->render('admin-contact.twig', $context);

//send_mail($_POST['contactEmail'], 'Contact Form Submission', $body);
//send_mail('eventsteam@bipsolutions.com', 'Admin Notification Contact Form Submission', $adminEmail);
//send_mail('gus.mackenzie@bipsolutions.com', 'Admin Notification Contact Form Submission', $adminEmail);
//send_mail('karen.milligan@bipsolutions.com', 'Admin Notification Contact Form Submission', $adminEmail);
send_mail('martin.malloy@bipsolutions.com', 'Admin Notification Contact Form Submission', $adminEmail);

$data = $_POST;

$contactTerms = isset($_POST['contactTerms']) ? 1 : 0 ;

DB::insert('contact_form_submissions', [
    //"contact_id" => $customer['id'],
    "contactName"  =>  $_POST['contactFirstName'] . ' ' . $_POST['contactLastName'],
    "contactCompany" => $_POST['contactCompany'],
    "contactEmail" => $_POST['contactEmail'],
    "contactEnquiry" => $_POST['contactEnquiry'],
    "contactOrgType" => $_POST['contactOrgType'],
    "contactMessage" => $_POST['contactMessage'],
    "contactTerms" => $contactTerms,
    "timestamp" => new DateTime("now")
]);

//echo $customer['id'];
?>
