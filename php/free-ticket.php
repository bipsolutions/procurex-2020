<?php

include '../static/dist/vendor/autoload.php';
require 'email.php';

\Stripe\Stripe::setApiKey('sk_live_ngik3vgZ1SC0b56iwTU83RTy00o1S4kUR8');

$insert = DB::insert('procurex_2020_tickets', array(
    'registrant_id' => $_POST['id'],
    'type' => 'free',
    'timestamp' => time()
));

//$customer = \Stripe\Customer::retrieve($_POST['id']);

$customer = DB::query("SELECT email from registrants where registrant_id =%s", $_POST['id']);

// $context['name'] = "Attendee";

$mailbody = $twig->render('free-ticket.twig');

if(isset($customer[0]['email'])){
    send_mail($customer[0]['email'], 'Thank you for registering for Procurex National 2020', $mailbody);
}

$adminEmail = $twig->render('admin-email.twig', $context);
send_mail('andrew.perry@bipsolutions.com', 'Admin Notification Free Ticket Submission', $adminEmail);

?>
<!-- <script>
    window.location.replace("http://www.w3schools.com");
</script> -->