<?php
include '../static/dist/vendor/autoload.php';

// Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
// $customer = $_POST['customer_data'];

$data = $_POST;

if (!isset($_POST['attendees'])) {
    $additional_attendees = array(
        "attendee_name" => isset($_POST['attendeeName']) ? $_POST['attendeeName'] : '',
        "attendee_email" => isset($_POST['attendeeEmail']) ? $_POST['attendeeEmail'] : '',
        "attendee_organisation" => isset($_POST['attendeeOrganisation']) ? $_POST['attendeeOrganisation'] : '',
    );
    $qty = 1;
} else {
    $additional_attendees = $_POST['attendees'];
    $qty = count($additional_attendees);
}
$additional_attendees = json_encode($additional_attendees);

\Stripe\Stripe::setApiKey('sk_test_gvE0UkPuP2thvTOyxf1lQWz6009efkshwv');

$customer = \Stripe\Customer::create([
    "name"  =>  $_POST['firstName'] . ' ' . $_POST['lastName'],
    "email" => $_POST['email'],
    "shipping" => array(
        "name" => $_POST['firstName'] . ' ' . $_POST['lastName'],
        "address" => array(
            "line1" => $_POST['address1'],
            "line2" => $_POST['address2'],
            "city" => $_POST['city'],
            "state" => $_POST['county'],
            "postal_code" => $_POST['postcode'],
        ),
        "phone" => $_POST['phone'],
    ),
    "metadata" => array(
        "orgName" => $_POST['organisationName'],
        "orgType" => $_POST['organisationType'],
        "orgPosition" => $_POST['position'],
        "referred" => $_POST['referred'],
        "businessType" => $_POST['businessType'],
        "industrySector" => $_POST['industrySector'],
        "orderNotes" => $_POST['orderNotes'],
        "delegates" => $additional_attendees,
        // "marketing" =>$_POST['marketing-input'], 
    ),
]);

$terms = isset($_POST['terms']) ? 1 : 0 ;
$marketingInput = isset($_POST['marketing-input']) ? 1 : 0 ;
$attendeeList = isset($_POST['attendeeList']) ? 1 : 0 ;
$disclaimer = isset($_POST['disclaimer']) ? 1 : 0 ;

DB::insert('registrants', [
    "registrant_id" => $customer['id'],
    "name"  =>  $_POST['firstName'] . ' ' . $_POST['lastName'],
    "email" => $_POST['email'],
    "shipping_address" => json_encode(array(
        "line1" => $_POST['address1'],
        "line2" => $_POST['address2'],
        "city" => $_POST['city'],
        "state" => $_POST['county'],
        "postal_code" => $_POST['postcode'],
    )),
    "phone" => $_POST['phone'],

    "orgName" => $_POST['organisationName'],
    "orgType" => $_POST['organisationType'],
    "orgPosition" => $_POST['position'],
    "referred" => $_POST['referred'],
    "businessType" => $_POST['businessType'],
    "industrySector" => $_POST['industrySector'],
    "ticketType" => $_POST['ticketType'],
    "orderNotes" => $_POST['orderNotes'],
    "terms" => $terms,
    "marketingInput" => $marketingInput,
    "attendeeList" => $attendeeList,
    "disclaimer" => $disclaimer,
    "attendees" => $additional_attendees,
    "timestamp" => new DateTime("now")
]);

$response = array(
    "customer" => $customer['id'],
    "qty" => $qty,
);

echo json_encode($response);
?>
