<?php
include './twig.php';

// Temporary measure for static data

#################### Speakers Start ####################

$data['speakers'][] = array(
    'speaker_id'    => '1', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/simonTse.jpg', 
        'alt' => '' 
    ), 
    'name'              => 'Simon Tse', 
    'company'           => '', 
    'position'          => 'Chief Executive of the Crown Commercial Service', 
    'bio'               => '<p>He first joined CCS in May 2016 and led the Procurement Operations directorate. He served as interim CEO from July 2018 until his permanent appointment, which followed an external competition, in December that year.</p><p>Simon is an experienced Chief Executive Officer and has extensive experience of strategic planning, customer service, driving performance improvement and achieving results. He first joined the Civil Service in 2008, taking up the position of Chief Executive Officer for the Driver and Vehicle Licensing Agency, where he remained in post until 2013.</p><p>Simon was Health Director at the Department for Work and Pensions, one of the UK’s largest public service departments, from 2013 - 2016. In this role he was responsible for the provision of all health and disability assessments services within the UK.</p><p>This role followed a successful career in the private sector spanning more than 25 years, most recently at Virgin Media, firstly as Managing Director for Wales and West, and subsequently as the UK SME Business Director.</p>', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '2', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/MalcolmHarrison.png', 
        'alt' => '' 
    ), 
    'name'              => 'Malcolm Harrison', 
    'company'           => '', 
    'position'          => 'Group CEO, CIPS', 
    'bio'               => '<p>Malcolm joined Mars in 1982 as a management trainee after graduating from Cambridge University with a degree in Chemical Engineering. He has a broad international experience in consumer facing companies in both general management and global functional roles, particularly Procurement. In his early career he worked in Production, Sales, Procurement, Supply Chain, and HR with Mars Confectionery, Pedigree Petfoods and Bass.  In 1993, he was appointed Purchasing Director of Bass Brewers where he established their first Purchasing function. From 1996 he was CEO of Bass Brewer’s UK Midlands free trade business before joining the Board of Britvic Soft Drinks in 1999 as Operations Director.</p><p>From 2000 he created and led global Procurement for Interbrew, participating in the acquisition and integration of several brewers – in Europe, Asia and Latin America - including AmBev in 2004; he was subsequently Chief Procurement Officer of InBev. In 2006 Malcolm joined Nestlé SA in Switzerland as Chief Procurement Officer responsible for the largest FMCG procurement spend in the world (CHF 60 Bn across 80 countries), heavily focused on global agricultural commodities; he brought a real focus to building the capability of the function and led a step-change in involvement of the procurement function with the Nestlé businesses. He joined the Executive Committee of Rexam plc, the FTSE 100 consumer packaging company, in 2010 as CEO of the global Plastic Packaging division (turnover of £ 1.3 Bn with 45 factories located in 9 countries) serving customers in Healthcare, Cosmetics and FMCG businesses. After leading its successful disposal, he worked in beverage cans during 2013 and his responsibilities included global IT, Environment Health & Safety and Global Procurement where managing the exposure to volatile Plastics and Metal prices was a key responsibility.</p><p>In 2015, Malcolm joined the Cabinet Office of Her Majesty’s Government where he held the role of Chief Executive Officer of the Crown Commercial Service, overseeing an annual spend of more than £13 Bn on common goods and services across the UK public sector. He also had responsibility for UK Public Sector Procurement Policy and for supporting the UK Government’s initiatives with SMEs.</p><p>In July 2018 he joined The Chartered Institute of Procurement & Supply (CIPS) as Group CEO.</p><p>Malcolm lives in the UK and has previously spent in total more than 12 years living in West Africa, France, Belgium and Switzerland as part of his professional career.</p>', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '3', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/MarkPalmer.png', 
        'alt' => '' 
    ), 
    'name'              => 'Mark Palmer', 
    'company'           => '', 
    'position'          => 'Google Cloud, Head of Public Sector, EMEA', 
    'bio'               => '', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '4', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/gillianAskew.jpg', 
        'alt' => '' 
    ), 
    'name'              => 'Gillian Askew', 
    'company'           => '', 
    'position'          => 'Director at All Things Procurement', 
    'bio'               => '<p>Gillian is an experienced procurement professional with a career spanning over 25 years in both private and public sector. Gillian currently runs All Things Procurement, a social enterprise focussing on increasing public sector business to SME\'s and optimising the delivery of social value through procurement.</p><p>Gillian is a Fellow of the Chartered Institute of Procurement and Supply and a member of the Institute of Leadership and Management, as well as a member of FSB and Social Value UK. She is also an active committee member of the CIPS South Yorkshire branch along with a number of other volunteering activities.</p><p>Gillian first joined public sector procurement in 2013 as Head of Procurement for Nottingham Trent University, she was also a non-executive director of NEUPC during her tenure there. She then joined YPO in 2016 as Head of Procurement Services responsible for over £600m public sector spend under management. Prior to this Gillian worked as Supply Chain Director within Babcock International Group leading global procurement and supply chain teams in asset infrastructure. In addition, Gillian has extensive experience as a small business owner including business acquisition and start up.</p>', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '5', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/bencarpenter.jpg', 
        'alt' => '' 
    ), 
    'name'              => 'Ben Carpenter', 
    'company'           => '', 
    'position'          => 'Chief executive of Social Value UK and Social Value International leading a global network of practitioners to develop principles and standards for social value accounting', 
    'bio'               => '<p>Ben Carpenter is the chief executive of Social Value UK and Social Value International leading a global network of practitioners to develop principles and standards for social value accounting.</p><p>Ben is part of the National Social Value Taskforce and the British Standards Industry committee for Social Value. Prior to becoming CEO, Ben was Social Value UK’s Operations Manager developing the international membership, the assurance and accreditation services and led on projects with the OECD, World Economic Forum, Impact Management Project and World Business Council for Sustainable Development. Ben’s background in homelessness, social housing and the built environment, affords him experience that spans sectors and an in-depth knowledge of Social Value accounting.</p>', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '6', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/chrisball.jpg', 
        'alt' => '' 
    ), 
    'name'              => 'Chris Ball', 
    'company'           => '', 
    'position'          => 'Research Associate Newcastle University Business School, Honorary Expert Associate for the National Innovation Centre on Ageing and Director of the Centre for Research into the Older Workforce', 
    'bio'               => '<p>Expert on all aspects of policy and practice regarding the ageing workforce. Passionate about the value of older workers, management of an age diverse workforce, development of the "silver economy" and demographic change, including international dimensions on all these. Speaker, commentator and writer.</p><p>Publications include "Managing the Ageing Workforce: An Introductory Guide for HR Professionals", (TAEN, 2007), research reports for Newcastle University and CROW, as well as academic book chapters and many blogs and articles.</p><p>Specialties: Learning, health and well-being, age management, labour market, demographic changes, employee relations.</p>', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '7', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/KevinOMalley.jpg', 
        'alt' => '' 
    ), 
    'name'              => 'Kevin O’Malley', 
    'company'           => '', 
    'position'          => 'Innovation Lead/ SBRI Lead, Clean Growth &amp; Infrastructure, Innovate UK', 
    'bio'               => '<p>Kevin is Innovation Lead/ SBRI Lead in the Clean Growth and Infrastructure Directorate at Innovate UK and is responsible for managing internal and external relationships, technology strategy and delivery with partners across central and local government, with the aim of boosting procurement-led innovation.</p><p>Kevin joined Innovate UK in 2018. Prior to coming to Innovate he was City Innovation Team Manager at Bristol City Council, leading the delivery of the Authorities award winning Smart City programme. He has been responsible for writing dozens of successful funding bids, including REPLICATE, Bristol’s £25m H2020 Smart Cities and Communities Lighthouse Project</p>', 
    'url'               => '', 
);

#################### Speakers End ####################

#################### Partners Start ####################

$data['participant_display_param'] = array(
    'display_title' => 1, 
    // 'custom_title_markup' => '', 
    'custom_title_markup' => '<div class="text-white"><h1 class="text-center">Event Partners</h1><hr class="hr-white hr-title mb-5 animated zoomIn"><div class="text-center"><p class="pb-2">Thank you to our partners</p></div></div>', 
    'title' => '', 
);

$data['partners'][] = array(
    'participant_id'    => '1', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo5.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.crowncommercial.gov.uk/', 
    'name'              => 'Crown Commercial Service', 
    'bio'               => '<p>CCS is the UK’s biggest public procurement organisation. We work closely with the MoD to save time and money buying business goods and services so they can focus on front-line services. Our solutions are free of charge, and help achieve maximum value by leveraging commercial expertise and national buying power.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '2', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo8.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.noecpc.nhs.uk/', 
    'name'              => 'NHS North of England Commercial Procurement Collaborative', 
    'bio'               => '<p>Established in 2007, and wholly owned by the NHS, NHS North of England Commercial Procurement Collaborative (NOE CPC) provides collaborative and bespoke procurement solutions to the NHS and other public sector organisations. Through category expertise and harnessing our collective buying power we deliver comprehensive, compliant and innovative procurement solutions which save the NHS money.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '3', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo2.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.cips.org/en-gb/', 
    'name'              => 'CIPS', 
    'bio'               => '<p>CIPS is the premier global organisation serving the procurement and supply profession, with a community of 118,000 in over 150 countries. Dedicated to promoting best practice, CIPS provides a wide range of services for the benefit of members and the wider business community.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '4', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo4.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.nepo.org/', 
    'name'              => 'NEPO', 
    'bio'               => '<p>NEPO provides a strategic collaborative procurement service to North East local authorities and delivers nationally available frameworks available for use by the wider public sector. NEPO’s compliant and value for money agreements cover a wide range of different categories including Specialist Professional Services, Agency Staff and Business Travel.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '5', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/ypo_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.ypo.co.uk/', 
    'name'              => 'YPO', 
    'bio'               => '<p>For 42 years YPO has supplied the public sector across the UK with products and services to help drive efficiency savings. We use our bulk buying power to achieve the best prices for our customers on supplies and for access to contract services – from everything to energy, insurance, ICT and HR. YPO is the largest formally constituted public sector buying organisation in the UK, with a total of 54 member authorities, meaning all profits go back into the public purse.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '6', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/mod_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.gov.uk/government/organisations/ministry-of-defence', 
    'name'              => 'MOD Doing Business With Defence', 
    'bio'               => '<p>We work for a secure and prosperous United Kingdom with global reach and influence. We will protect our people, territories, values and interests at home and overseas, through strong armed forces and in partnership with allies, to ensure our security, support our national interests and safeguard our prosperity. MOD is a ministerial department, supported by 27 agencies and public bodies.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '7', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/lga_logo.jpg',
        'alt' => ''
    ), 
    'url'               => 'https://www.local.gov.uk/', 
    'name'              => 'Local Government Association (LGA)', 
    'bio'               => '<p>LGA is the national voice of local government. We work with councils to support, promote and improve local government. We are a politically led, cross-party organisation that works on behalf of councils to ensure local government has a strong, credible voice with national government. In total 411 local authorities are members of the LGA.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '8', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/ukupc_logo.png',
        'alt' => ''
    ), 
    'url'               => 'http://www.apuc-scot.ac.uk/', 
    'name'              => 'UKUPC (UK Universities Purchasing Consortia)', 
    'bio'               => '<p>UKUPC (UK Universities Purchasing Consortia) are the evolved convergence of eight UK Consortia; APUC, HEPCW, LUPC, NEUPC, NWUPC, SUPC, TEC and TUCO who created a formal partnership to support collaborative procurement within Higher Education.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '9', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/pass_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.passprocurement.co.uk/', 
    'name'              => 'Procurement Advice and Support Service (PASS)', 
    'bio'               => '<p>PASS provides expert public procurement training and support for both public and private sector organisations. Whether your goal is to maximise efficiencies or increase your organisation’s chances of tendering success, PASS will ensure you are equipped with the knowledge you need. Discover how you can Learn, Develop and Accomplish with PASS.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '10', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/delta_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.delta-esourcing.com/', 
    'name'              => 'Delta eSourcing', 
    'bio'               => '<p>Delta eSourcing enables efficient, effective and compliant procurement. Utilised by thousands of public sector buyers every day, its Buyer Portal, Tender Manager, Supplier Manager, Contract Manager and eAuctions services can be used independently or else combined to form a comprehensive and effective end-to-end procurement solution. In challenging times, Delta eSourcing delivers transparency, compliance and value for money.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '11', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/supply2gov_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.supply2govtenders.co.uk/', 
    'name'              => 'Supply2Gov', 
    'bio'               => '<p>At Supply2Gov, we have one goal – to make business growth simpler for sole traders, micro and small businesses. Powered by the UK and Republic of Ireland’s largest database of public sector contract notices and awards, combined with our daily email alerts sent straight to your inbox, we’ve made it as easy as possible for you to find relevant opportunities as soon as they become available – giving you more time to focus on putting your bids together and growing your business. You can register for a free local area of your choice or take advantage of our flexible monthly payment options – giving you a no risk, scalable, cost effective contract alerts service option.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '12', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/tracker_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.trackerintelligence.com/', 
    'name'              => 'Tracker', 
    'bio'               => '<p>Tracker is the only end-to-end business development solution with the unique intelligence you need to find, bid for and win more business. With access to Europe’s largest database of opportunities and competitive insights – you can engage earlier to sell more effectively and win more business. And Tracker’s just got even better – now you can also upload opportunity documents and manage your bid responses all in the one place. Focus on winning business – not looking for it.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

#################### Partners End ####################

render(null, $data);
?>
