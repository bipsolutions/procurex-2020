<?php
include '../twig.php';

$data['content'] =  "<h1>This is the content for the overview page</h1>";

// $data['content'] = array(
//     "content" => "This is the content for the overview page",
//     "more content" => "This is even more content for the overview page"
// );

#################### Partners Start ####################

$data['participant_display_param'] = array(
    'display_title' => 1, 
    // 'custom_title_markup' => '', 
    'custom_title_markup' => '<div class="text-white"><h1 class="text-center">Event Partners</h1><hr class="hr-white hr-title mb-5 animated zoomIn"><div class="text-center"><p class="pb-2">Thank you to our partners</p></div></div>', 
    'title' => '', 
);

$data['partners'][] = array(
    'participant_id'    => '1', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo5.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.crowncommercial.gov.uk/', 
    'name'              => 'Crown Commercial Service', 
    'bio'               => '<p>CCS is the UK’s biggest public procurement organisation. We work closely with the MoD to save time and money buying business goods and services so they can focus on front-line services. Our solutions are free of charge, and help achieve maximum value by leveraging commercial expertise and national buying power.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '2', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo8.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.noecpc.nhs.uk/', 
    'name'              => 'NHS North of England Commercial Procurement Collaborative', 
    'bio'               => '<p>Established in 2007, and wholly owned by the NHS, NHS North of England Commercial Procurement Collaborative (NOE CPC) provides collaborative and bespoke procurement solutions to the NHS and other public sector organisations. Through category expertise and harnessing our collective buying power we deliver comprehensive, compliant and innovative procurement solutions which save the NHS money.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '3', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo2.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.cips.org/en-gb/', 
    'name'              => 'CIPS', 
    'bio'               => '<p>CIPS is the premier global organisation serving the procurement and supply profession, with a community of 118,000 in over 150 countries. Dedicated to promoting best practice, CIPS provides a wide range of services for the benefit of members and the wider business community.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '4', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo4.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.nepo.org/', 
    'name'              => 'NEPO', 
    'bio'               => '<p>NEPO provides a strategic collaborative procurement service to North East local authorities and delivers nationally available frameworks available for use by the wider public sector. NEPO’s compliant and value for money agreements cover a wide range of different categories including Specialist Professional Services, Agency Staff and Business Travel.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '5', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/ypo_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.ypo.co.uk/', 
    'name'              => 'YPO', 
    'bio'               => '<p>For 42 years YPO has supplied the public sector across the UK with products and services to help drive efficiency savings. We use our bulk buying power to achieve the best prices for our customers on supplies and for access to contract services – from everything to energy, insurance, ICT and HR. YPO is the largest formally constituted public sector buying organisation in the UK, with a total of 54 member authorities, meaning all profits go back into the public purse.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '6', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/mod_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.gov.uk/government/organisations/ministry-of-defence', 
    'name'              => 'MOD Doing Business With Defence', 
    'bio'               => '<p>We work for a secure and prosperous United Kingdom with global reach and influence. We will protect our people, territories, values and interests at home and overseas, through strong armed forces and in partnership with allies, to ensure our security, support our national interests and safeguard our prosperity. MOD is a ministerial department, supported by 27 agencies and public bodies.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '7', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/lga_logo.jpg',
        'alt' => ''
    ), 
    'url'               => 'https://www.local.gov.uk/', 
    'name'              => 'Local Government Association (LGA)', 
    'bio'               => '<p>LGA is the national voice of local government. We work with councils to support, promote and improve local government. We are a politically led, cross-party organisation that works on behalf of councils to ensure local government has a strong, credible voice with national government. In total 411 local authorities are members of the LGA.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '8', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/ukupc_logo.png',
        'alt' => ''
    ), 
    'url'               => 'http://www.apuc-scot.ac.uk/', 
    'name'              => 'UKUPC (UK Universities Purchasing Consortia)', 
    'bio'               => '<p>UKUPC (UK Universities Purchasing Consortia) are the evolved convergence of eight UK Consortia; APUC, HEPCW, LUPC, NEUPC, NWUPC, SUPC, TEC and TUCO who created a formal partnership to support collaborative procurement within Higher Education.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '9', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/pass_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.passprocurement.co.uk/', 
    'name'              => 'Procurement Advice and Support Service (PASS)', 
    'bio'               => '<p>PASS provides expert public procurement training and support for both public and private sector organisations. Whether your goal is to maximise efficiencies or increase your organisation’s chances of tendering success, PASS will ensure you are equipped with the knowledge you need. Discover how you can Learn, Develop and Accomplish with PASS.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '10', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/delta_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.delta-esourcing.com/', 
    'name'              => 'Delta eSourcing', 
    'bio'               => '<p>Delta eSourcing enables efficient, effective and compliant procurement. Utilised by thousands of public sector buyers every day, its Buyer Portal, Tender Manager, Supplier Manager, Contract Manager and eAuctions services can be used independently or else combined to form a comprehensive and effective end-to-end procurement solution. In challenging times, Delta eSourcing delivers transparency, compliance and value for money.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '11', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/supply2gov_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.supply2govtenders.co.uk/', 
    'name'              => 'Supply2Gov', 
    'bio'               => '<p>At Supply2Gov, we have one goal – to make business growth simpler for sole traders, micro and small businesses. Powered by the UK and Republic of Ireland’s largest database of public sector contract notices and awards, combined with our daily email alerts sent straight to your inbox, we’ve made it as easy as possible for you to find relevant opportunities as soon as they become available – giving you more time to focus on putting your bids together and growing your business. You can register for a free local area of your choice or take advantage of our flexible monthly payment options – giving you a no risk, scalable, cost effective contract alerts service option.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

$data['partners'][] = array(
    'participant_id'    => '12', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/tracker_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.trackerintelligence.com/', 
    'name'              => 'Tracker', 
    'bio'               => '<p>Tracker is the only end-to-end business development solution with the unique intelligence you need to find, bid for and win more business. With access to Europe’s largest database of opportunities and competitive insights – you can engage earlier to sell more effectively and win more business. And Tracker’s just got even better – now you can also upload opportunity documents and manage your bid responses all in the one place. Focus on winning business – not looking for it.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => ''
);

#################### Partners End ####################

render('overview', $data);
