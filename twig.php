<?php
require_once 'static/dist/vendor/autoload.php';

$rootPath   =  getenv('px_path');
$siteUrl    =  getenv('px_url');

$loader = new \Twig\Loader\FilesystemLoader('templates', $rootPath);
$loader->addPath('templates/parts');
$loader->addPath('templates/sections');

$twig = new \Twig\Environment($loader);
$menu = json_decode(file_get_contents($rootPath.'/json/menus.json'));
$event = json_decode(file_get_contents($rootPath.'/json/event.json'));

$context['event'] = $event;
$context['menus'] = $menu; 


$backtrace = debug_backtrace();
$template = basename($backtrace[0]['file'], '.php');

$context['site'] = array(
    "path" => $rootPath,
    "url"   => $siteUrl,
    'slug'  => $template
);

$function_filedate = new \Twig\TwigFunction(
    'fileDate',
    /**
     * @param $file_path
     * This function generates a new file path with the last date of filechange
     * to support better better client caching via Expires header:
     * i.e:
     * css/style.css -> css/style.1428423235.css
     *
	 * Usage in template files:
	 * i.e:
	 * <link rel="stylesheet" href="{{ fileDate('css/style.css') }}">
     * 
     * For local testing might want to change the below line to (due to the way document root is retrieved locally)
     * $change_date = @filemtime($_SERVER['DOCUMENT_ROOT'].'/'.'procurex2020/'.$file_path); 
	 *
     * Apache Rewrite Rule:
     *
     * RewriteCond %{REQUEST_FILENAME} !-f
     * RewriteCond %{REQUEST_FILENAME} !-d
     * RewriteRule ^(.*)\.[\d]{10}\.(css|js)$ $1.$2 [NC,L]
     *
     * MUST BE INITIATED/ADDED IN THE ROOT DIRECTORY TO GET CORRECT PATH (in case of WordPress theme root)
     *
     * @return mixed
     */
    function ($file_path) {
        $change_date = @filemtime(__DIR__ . '/' . $file_path);

        if (!$change_date) {
            //Fallback if mtime could not be found:
            $change_date = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        }
        return preg_replace('{\\.([^./]+)$}', ".$change_date.\$1", $file_path);
    }
);
$twig->addFunction($function_filedate);
    

function render($template = null, $context = null){
    global $twig;

    $context = (!empty($context)) ? array_merge($GLOBALS['context'], $context) : $GLOBALS['context'] ;

    if(!$template){
        $backtrace = debug_backtrace();
        $template = basename($backtrace[0]['file'], '.php');
    }

    
    echo $twig->render($template.'.twig', $context);
}
