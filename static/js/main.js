// Marketing Opt in for Sole Trader / Partnership to show checkbox
$("#marketing-input").hide();
$("#marketing-label").hide();

$("#organisationType").change(function () {
  var text = $('#organisationType :selected').text();

  if (text === 'Sole Trader' || text === 'Other Type of Partnership') {
    $("#marketing-input").show();
    $("#marketing-label").show();
  } else {
    $("#marketing-input").hide();
    $("#marketing-label").hide();
  }
});

$("#contactTerms").hide();
$("#contactTerms-label").hide();

$("#contactOrgType").change(function () {
  var text = $('#contactOrgType :selected').text();

  if (text === 'Sole Trader' || text === 'Other Type of Partnership') {
    $("#contactTerms").show();
    $("#contactTerms-label").show();
  } else {
    $("#contactTerms").hide();
    $("#contactTerms-label").hide();
  }
});

$('.fas.fa-minus-circle').hide();

$('.md-accordion').on('shown.bs.collapse', function () {
  $('.fas.fa-plus-circle').hide();
  $('.fas.fa-minus-circle').show();
});

$('.md-accordion').on('hidden.bs.collapse', function () {
  $('.fas.fa-minus-circle').hide();
  $('.fas.fa-plus-circle').show();
});

$(document).ready(function () {

  // Gets the video src from the data-src on each button

  var $videoSrc;
  $('.video-btn').click(function () {
    $videoSrc = $(this).data("src");
  });
  //console.log($videoSrc);



  // when the modal is opened autoplay it  
  $('#myModal').on('shown.bs.modal', function (e) {

    // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
    $("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
  })



  // stop playing the youtube video when I close the modal
  $('#myModal').on('hide.bs.modal', function (e) {
    // a poor man's stop video
    $("#video").attr('src', $videoSrc);
  })



  $(function () {
    var hash = window.location.hash;
    //console.log(hash);
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.nav-tabs a').click(function (e) {
      $(this).tab('show');
      var scrollmem = $('body').scrollTop();
      window.location.hash = this.hash;
      $('html,body').scrollTop(scrollmem);
    });
  });

  var wow = new WOW(
    {
      boxClass: 'wow',      // animated element css class (default is wow)
      animateClass: 'animated', // animation css class (default is animated)
      offset: 0,          // distance to the element when triggering the animation (default is 0)
      mobile: false,       // trigger animations on mobile devices (default is true)
      live: true,       // act on asynchronously loaded content (default is true)
      callback: function (box) {
        // the callback is fired every time an animation is started
        // the argument that is passed in is the DOM node being animated
      },
      scrollContainer: null // optional scroll container selector, otherwise use window
    }
  );
  wow.init();

  $("#myCarousel").on("slide.bs.carousel", function (e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 4;
    var totalItems = $(".carousel-item").length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i = 0; i < it; i++) {
        // append slides to end
        if (e.direction == "left") {
          $(".carousel-item")
            .eq(i)
            .appendTo(".carousel-inner");
        } else {
          $(".carousel-item")
            .eq(0)
            .appendTo($(this).find(".carousel-inner"));
        }
      }
    }
  });

  if($('#sponsors_carousel .carousel-item').length > 4){
    $('#sponsors_carousel').carousel({
      interval: 2000, 
      ride: 'carousel', 
      pause: false
    });

  }

  if($('#speakers_carousel .carousel-item').length > 4){
    $('#speakers_carousel').carousel({
      interval: 2000, 
      ride: 'carousel'
    });

  }


  // Applies endless carousel effect to all carousels with a class (assumes 4 display items)
  $('.carousel_4_endless').each(function(){

    // Run on carousel slide
    $(this).on('slide.bs.carousel', function(e){

      var $e = $(e.relatedTarget);
      var idx = $e.index();
  
      var carousel_inner = $(this).find('.carousel-inner');
      var carousel_items = $(this).find('.carousel-item');
  
      // Number of viewable/visible items. Important. To be modified as needed.
      var visibleItems = 4;
      var totalItems = carousel_items.length;
      
      // Set the position of "active" slide. Includes the amount of visible slides. After that appends new on slide
      if(idx >= totalItems - (visibleItems - 1)){
  
        // Number of items to append. Normally will be 1 unless backwards slide
        var it = visibleItems - (totalItems - idx);
  
        // Append slides to end
        for(var i=0; i<it; i++){
  
          // Appends the first slide of carousel to the end
          carousel_items.eq(0).appendTo(carousel_inner);
  
        }
  
      }

    });

  });

});

// Initiate bootstrap tooltips
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});

// #################### Sales brochure form validation controls start ####################

$(document).ready(function(){

  // Agenda overview toggle click handler
  $('.agenda .row').on('click', '.toggle-collapse', function(e){

    var collapse_element = $(e.target.getAttribute('data-target'));
    
    // Prevents modal pop up (since that covers entire row)
    e.stopPropagation();

    collapse_element.siblings('.collapse').collapse('hide');
    collapse_element.collapse('toggle');

    // Change the toggle icon from plus to minus
    collapse_element.on('shown.bs.collapse', function () {
      $(e.target).removeClass('fa-plus-circle').addClass('fa-minus-circle');;
    });
    
    // Change the toggle icon from minus to plus
    collapse_element.on('hidden.bs.collapse', function () {
      $(e.target).removeClass('fa-minus-circle').addClass('fa-plus-circle');
    });

  });

  // To be called on document ready
  $(document).on('focusout', '.brochure_input', function(event){

    var field_id = event.target.id;
    var field_value = event.target.value;
    var function_name = 'validate_' + field_id;

    // Call The appropiate validation method
    var is_valid = call_validation(function_name, field_value);
    var field = event.target;

    // Mark the field depending on its validity
    mark_field_validity(is_valid, field);

  });

  // Sales brochure download button controls. 
  $(document).on('click', '#storeBrochureDetails', function(event){

    var allow_submit = true;
    var form_fields = $('.brochure_input');

    $('#form_error').hide();

    $.each(form_fields, function(index, element){

      var field_value = element.value;
      var function_name = 'validate_' + element.id;
      var is_valid = call_validation(function_name, field_value);
      mark_field_validity(is_valid, element);

      if(allow_submit && !is_valid){

        allow_submit = false;

      }

    });

    // If field validation passes, submit the form for download. 
    if(allow_submit){

      $(event.target.form).submit();

    }else{

      $('#form_error').fadeIn();
      $('body,html').animate(
        {
            scrollTop: $("#brochure-form").offset().top - 100
        },
        800 //speed
        );
    }

  });

});

// Selects either default validation to check if field is empty or specified validation function and returns the result
function call_validation(function_name, field_value){
  if(typeof window[function_name] === "function"){
    return window[function_name](field_value);
  }else{
    return not_empty(field_value);
  }
}

// Marks fields green if valid and red if invalid
function mark_field_validity(is_valid, field){

  // Remove previous status
  $(field).removeClass('is-valid');
  $(field).removeClass('is-invalid');

  // Add current status
  if(is_valid){
    $(field).addClass('is-valid');
  }else{
    $(field).addClass('is-invalid');
  }

}

function not_empty(input){
  if(input.length == 0){
    return false;
  } else {
    return true;
  }
}

function validate_brochureName(input){
  return not_empty(input);
}

function validate_brochureEmail(input){
  var re = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
  return re.test(input.toLowerCase());
}

function validate_brochureCompany(input){
  return not_empty(input);
}

function validate_brochurePhone(input){
  var re = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]*$/;
  return re.test(input.toLowerCase());
}

// #################### Sales brochure form validation controls end ####################
