function get_max_height_from_set(classSet){

    var maxHeight = Math.max.apply(null, jQuery(classSet).map(function (){

        return jQuery(this).height();

    }).get());

    jQuery(classSet).each(function(){

        jQuery(this).height(maxHeight);

    });

}
function get_max_width_from_set(classSet){

    var maxWidth = Math.max.apply(null, jQuery(classSet).map(function (){

        return jQuery(this).width();

    }).get());

    jQuery(classSet).each(function(){

        jQuery(this).width(maxWidth);

    });

}

function get_min_height_from_set(classSet){

    var maxHeight = Math.min.apply(null, jQuery(classSet).map(function (){

        return jQuery(this).height();

    }).get());

    jQuery(classSet).each(function(){

        jQuery(this).height(maxHeight);

    });

}

function get_min_width_from_set(classSet){

    var maxWidth = Math.min.apply(null, jQuery(classSet).map(function (){

        return jQuery(this).width();

    }).get());

    jQuery(classSet).each(function(){

        jQuery(this).width(maxWidth);

    });

}

function bip_vertically_align_set(target, outer){

    jQuery(target).each(function(){

        var current         = jQuery(this);
        var the_outer       = jQuery(current.closest(outer));

        // to get the width of a border you must provide an explicit side of the border
        var currentBorder   = the_outer.css('border-left-width');

        var outerHeight     = the_outer.outerHeight();
        var innerHeight     = current.outerHeight();

        current.css('display', 'inline-block').css('padding', '0').css('margin', '0');
        the_outer.css('display', 'inline-block').css('margin', '0').css('padding', currentBorder);

        var margin = (outerHeight - innerHeight) / 2;

        current.css('margin-top', margin);

    });

}