<?php
include './twig.php';

/**
 * Temporarily (likely) commented out to provide static data instead. To be reactivated as needed. 
 */
// $ch = curl_init();

// // $dataset = array('post_type' => 'test101');
// $params = array(
//     'post_type'         => 'test101',
//     'taxonomy'          => array(
//         array(
//             'taxonomy'  => 'test101_taxonomy', 
//             'field'    => 'slug', 
//             'terms'    => 'procurex_national', 
//         ),
//     ),
//     'participant_type'  => 'exhibitor',
// );

// $curl_options = array(
//     CURLOPT_URL => 'http://localhost/bip/wp-json/bip_headless/v1/get_participants' . '?' . http_build_query($params), 
//     CURLOPT_RETURNTRANSFER => true, 
// );

// curl_setopt_array($ch, $curl_options);

// $response = curl_exec($ch);

// curl_close($ch);

// $data['exhibitors'] = json_decode($response, true);

#################### Exhibitors Start ####################

$data['participant_display_param'] = array(
    'display_title' => 1, 
    'custom_title_markup' => '', 
    // 'custom_title_markup' => '<h1 class="text-center">Event Partners</h1><hr class="hr-white hr-title mb-5 animated zoomIn"><div class="text-center"><p class="pb-2">Thank you to our partners</p></div>', 
    'title' => 'Exhibitors', 
);

$data['participants'][] = array(
    'participant_id'    => '1', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/fusion21_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.fusion21.co.uk/', 
    'name'              => 'Fusion21 Ltd', 
    'bio'               => '<p>Fusion21 helps people buy smarter in the public sector and deliver on their social impact goals. We’ve saved our members more than £226m with fully compliant procurement frameworks, while the social value of our projects currently stands at £84m. Talk to us about making a real impact.</p>', 
    'framework'         => false, 
    'stand_number'      => '29', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '2', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/bagnal_and_morris_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.bagnallandmorris.com/', 
    'name'              => 'Bagnall & Morris Waste Services Ltd', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '21', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '3', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/consortium_procurement_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://consortiumprocurement.org.uk/', 
    'name'              => 'Consortium Procurement', 
    'bio'               => '<p>Consortium Procurement is the national procurement service of the Northern Housing Consortium, providing a wide range of framework agreements covering compliance management, asset management, telecare and financial inclusion including insurance and electronic payments. We currently represent over 300 members who between them manage over three million social housing tenancies across the UK.</p>', 
    'framework'         => false, 
    'stand_number'      => '83', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '4', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/oxford_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.oxfordprofessionaleducationgroup.com/', 
    'name'              => 'Oxford Professional Education Group', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '13', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '5', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/orbis_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.orbisprotect.com/', 
    'name'              => 'Orbis', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '77', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '6', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/inventry_logo.jpg',
        'alt' => ''
    ), 
    'url'               => 'https://inventry.co.uk/', 
    'name'              => 'InVentry Limited', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '40', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '7', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/banner_logo.jpg',
        'alt' => ''
    ), 
    'url'               => 'https://www.banneruk.com/', 
    'name'              => 'Banner', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '38', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '8', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/alcatel_lucent_logo.jpg',
        'alt' => ''
    ), 
    'url'               => 'https://sol-distribution.co.uk/alcatel-lucent-enterprise/', 
    'name'              => 'Alcatel-Lucent and Sol Distribution', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '11', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '9', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/office_depot_logo.jpg',
        'alt' => ''
    ), 
    'url'               => 'https://online.officedepot.co.uk/', 
    'name'              => 'Office Depot', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '39', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '10', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/ascertia_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.ascertia.com/', 
    'name'              => 'Ascertia Limited', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '8', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '11', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/sr_logo.JPG',
        'alt' => ''
    ), 
    'url'               => 'https://www.srscc.co.uk/', 
    'name'              => 'SR Supply Chain Consultants Ltd', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '76', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '12', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/berthold_bauer_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://bbvat.co.uk/author/bbvat/', 
    'name'              => 'Berthold Bauer', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '49', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '13', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/procurement_hub_logo.jpg',
        'alt' => ''
    ), 
    'url'               => 'https://www.procurementhub.co.uk/', 
    'name'              => 'Procurement Hub', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '32', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '14', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/signlive_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://signlive.co.uk/', 
    'name'              => 'SignLive Ltd', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '62', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '15', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/sls_media_logo.png',
        'alt' => ''
    ), 
    'url'               => 'http://www.slsmedia.co.uk/', 
    'name'              => 'SLS Media Limited', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '79', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '16', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/augmentas_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.augmentasgroup.com/', 
    'name'              => 'Augmentas Group Limited', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '30', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '17', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/hague_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.hagueprint.com/our-services/software', 
    'name'              => 'Hague Software Solutions Ltd', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '31', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '18', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/gemserv_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.gemserv.com/', 
    'name'              => 'Gemserv Limited', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '35', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '19', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/ctm_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.travelctm.co.uk/', 
    'name'              => 'Corporate Travel Management', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '74', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '20', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/nippon_gases_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://nippongases.com/', 
    'name'              => 'Nippon  Gases Offshore Ltd', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '63', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '21', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/mod_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.gov.uk/government/organisations/ministry-of-defence/about/procurement', 
    'name'              => 'MOD Doing Business With Defence', 
    'bio'               => '<p>CCS is the UK’s biggest public procurement organisation. We work closely with the MoD to save time and money buying business goods and services so they can focus on front-line services. Our solutions are free of charge, and help achieve maximum value by leveraging commercial expertise and national buying power.</p>', 
    'framework'         => false, 
    'stand_number'      => '80', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '22', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/capito_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.capito.co.uk/', 
    'name'              => 'Capito Limited', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '14', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '23', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/cloud_digital_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.clouddigital-emea.com/', 
    'name'              => 'Cloud Digital Limited', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '34', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '24', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/lyreco_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.lyreco.com/webshop/ENEN/index.html', 
    'name'              => 'Lyreco', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '42', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '25', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/luminate_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.luminite.co.uk/', 
    'name'              => 'Luminite', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '26', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/ukupc_logo.png',
        'alt' => ''
    ), 
    'url'               => 'http://www.apuc-scot.ac.uk/', 
    'name'              => 'UKUPC (UK Universities Purchasing Consortia)', 
    'bio'               => '<p>UKUPC (UK Universities Purchasing Consortia) are the evolved convergence of eight UK Consortia; APUC, HEPCW, LUPC, NEUPC, NWUPC, SUPC, TEC and TUCO who created a formal partnership to support collaborative procurement within Higher Education.</p>', 
    'framework'         => false, 
    'stand_number'      => '85', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '27', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/sortimo_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.mysortimo.co.uk/en_UK/', 
    'name'              => 'Sortimo', 
    'bio'               => '', 
    'framework'         => false, 
    'stand_number'      => '44', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '28', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/circular_computing_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.circularcomputing.com/', 
    'name'              => 'Circular Computing', 
    'bio'               => '<p>Value. Performance. Sustainability. Circular Computing satisfy all the key requirements of public procurement, offering completely carbon-neutral enterprise-grade laptops through a unique remanufacturing process. A council in the South West has already saved £250,000 and proved their green credentials, and the movement is growing. Because IT shouldn’t cost the Earth.</p>', 
    'framework'         => false, 
    'stand_number'      => '41', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '29', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo5.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.crowncommercial.gov.uk/', 
    'name'              => 'Crown Commercial Service', 
    'bio'               => '<p>CCS is the UK’s biggest public procurement organisation. We work closely with the MoD to save time and money buying business goods and services so they can focus on front-line services. Our solutions are free of charge, and help achieve maximum value by leveraging commercial expertise and national buying power.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '30', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo8.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.noecpc.nhs.uk/', 
    'name'              => 'NHS North of England Commercial Procurement Collaborative', 
    'bio'               => '<p>Established in 2007, and wholly owned by the NHS, NHS North of England Commercial Procurement Collaborative (NOE CPC) provides collaborative and bespoke procurement solutions to the NHS and other public sector organisations. Through category expertise and harnessing our collective buying power we deliver comprehensive, compliant and innovative procurement solutions which save the NHS money.</p>', 
    'framework'         => false, 
    'stand_number'      => '5', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '31', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo2.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.cips.org/en-gb/', 
    'name'              => 'CIPS', 
    'bio'               => '<p>CIPS is the premier global organisation serving the procurement and supply profession, with a community of 118,000 in over 150 countries. Dedicated to promoting best practice, CIPS provides a wide range of services for the benefit of members and the wider business community.</p>', 
    'framework'         => false, 
    'stand_number'      => '81', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '32', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/logo4.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.nepo.org/', 
    'name'              => 'NEPO', 
    'bio'               => '<p>NEPO provides a strategic collaborative procurement service to North East local authorities and delivers nationally available frameworks available for use by the wider public sector. NEPO’s compliant and value for money agreements cover a wide range of different categories including Specialist Professional Services, Agency Staff and Business Travel.</p>', 
    'framework'         => false, 
    'stand_number'      => '67', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '33', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/ypo_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.ypo.co.uk/', 
    'name'              => 'YPO', 
    'bio'               => '<p>For 42 years YPO has supplied the public sector across the UK with products and services to help drive efficiency savings. We use our bulk buying power to achieve the best prices for our customers on supplies and for access to contract services – from everything to energy, insurance, ICT and HR. YPO is the largest formally constituted public sector buying organisation in the UK, with a total of 54 member authorities, meaning all profits go back into the public purse.</p>', 
    'framework'         => false, 
    'stand_number'      => '16', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '34', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/lga_logo.jpg',
        'alt' => ''
    ), 
    'url'               => 'https://www.local.gov.uk/', 
    'name'              => 'Local Government Association (LGA)', 
    'bio'               => '<p>LGA is the national voice of local government. We work with councils to support, promote and improve local government. We are a politically led, cross-party organisation that works on behalf of councils to ensure local government has a strong, credible voice with national government. In total 411 local authorities are members of the LGA.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '35', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/pass_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.passprocurement.co.uk/', 
    'name'              => 'Procurement Advice and Support Service (PASS)', 
    'bio'               => '<p>PASS provides expert public procurement training and support for both public and private sector organisations. Whether your goal is to maximise efficiencies or increase your organisation’s chances of tendering success, PASS will ensure you are equipped with the knowledge you need. Discover how you can Learn, Develop and Accomplish with PASS.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '36', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/delta_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.delta-esourcing.com/', 
    'name'              => 'Delta eSourcing', 
    'bio'               => '<p>Delta eSourcing enables efficient, effective and compliant procurement. Utilised by thousands of public sector buyers every day, its Buyer Portal, Tender Manager, Supplier Manager, Contract Manager and eAuctions services can be used independently or else combined to form a comprehensive and effective end-to-end procurement solution. In challenging times, Delta eSourcing delivers transparency, compliance and value for money.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '37', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/supply2gov_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.supply2govtenders.co.uk/', 
    'name'              => 'Supply2Gov', 
    'bio'               => '<p>At Supply2Gov, we have one goal – to make business growth simpler for sole traders, micro and small businesses. Powered by the UK and Republic of Ireland’s largest database of public sector contract notices and awards, combined with our daily email alerts sent straight to your inbox, we’ve made it as easy as possible for you to find relevant opportunities as soon as they become available – giving you more time to focus on putting your bids together and growing your business. You can register for a free local area of your choice or take advantage of our flexible monthly payment options – giving you a no risk, scalable, cost effective contract alerts service option.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => 'exhibitor'
);

$data['participants'][] = array(
    'participant_id'    => '38', 
    'logo'              => array(
        'url' => getenv('px_url') . 'static/img/logos/tracker_logo.png',
        'alt' => ''
    ), 
    'url'               => 'https://www.trackerintelligence.com/', 
    'name'              => 'Tracker', 
    'bio'               => '<p>Tracker is the only end-to-end business development solution with the unique intelligence you need to find, bid for and win more business. With access to Europe’s largest database of opportunities and competitive insights – you can engage earlier to sell more effectively and win more business. And Tracker’s just got even better – now you can also upload opportunity documents and manage your bid responses all in the one place. Focus on winning business – not looking for it.</p>', 
    'framework'         => false, 
    'stand_number'      => '', 
    'participant_type'  => 'exhibitor'
);

// Alphabetical sorting
usort($data['participants'], function($a, $b){ return strcasecmp($a["name"], $b["name"]); });

#################### Exhibitors End ####################

render('participant_gallery_page', $data);
?>
