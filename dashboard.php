<?php
include './twig.php';

  
  /**
 * Export an array of data into a csv file
 *
 * @param string $fileName The name you want for the file   
 * @param array $assocDataArray and array of data to be added to the file.
 * @return void
 */

function ho_outputCsv( $fileName, $assocDataArray ) {
  ob_clean();
  header( 'Pragma: public' );
  header( 'Expires: 0' );
  header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
  header( 'Cache-Control: private', false );
  header( 'Content-Type: text/csv' );
  header( 'Content-Disposition: attachment;filename=' . $fileName );
  if ( isset( $assocDataArray['0'] ) ) {
      $fp = fopen( 'php://output', 'w' );
      fputcsv( $fp, array_keys( $assocDataArray['0'] ) );
      foreach ( $assocDataArray AS $values ) {
          fputcsv( $fp, $values );
      }
      fclose( $fp );
  }
  ob_flush();
}

if(isset($_POST['download'])) { 
  include_once 'registrants.php';
  //ho_outputCsv('registrants.csv', $result);
  die();
} 

if(isset($_POST['download-brochure'])) { 
  include_once 'download-brochure.php';
  //ho_outputCsv('salesbrochure.csv', $salesResult);
  die();
}

if(isset($_POST['contact-data'])) { 
  include_once 'contact-data.php';
  //ho_outputCsv('contact-data.csv', $contactResult);
  die();
}

render('dashboard');
