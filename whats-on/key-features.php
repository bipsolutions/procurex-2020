<?php
include '../twig.php';

// $context['content'] = array(
//     1 => "Content block 1",
//     2 => "Content block 2"
// ); 
// then, in TWIG:
//  {{content.1}}

//OR

// $context['content'] = array(
//     "overview" => "overview content",
//     "second_level_name" => "second level content"
// ); 
// then, in TWIG:
//  {{content.overview}}

// OR

// $context['content'] = $string
// then, in TWIG:
//  {{content}}

//and render like:
// render('page', $context);


$context['blocks'] = array(
    "welcome" => "Procurex National is one of the major highlights within the annual public procurement calendar, supporting innovation, education, collaboration and celebration over a single dedicated day.",
    "visiting" => array(
        0 => array(
            "icon" => "http://src",
            "url" => "http://",
            "title" => "Attending",
            "content" => "content"
        ),
        1 => array(
            "icon" => "http://src",
            "url" => "http://",
            "title" => "Sponsoring",
            "content" => "content"
        ),
        2 => array(
            "icon" => "http://src",
            "url" => "http://",
            "title" => "Book now",
            "content" => "content"
        )
    )
); 

// Temporary measure for static data
$data['speakers'][] = array(
    'speaker_id'    => '1', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/simonTse.jpg', 
        'alt' => '' 
    ), 
    'name'              => 'Simon Tse', 
    'company'           => '', 
    'position'          => 'Chief Executive of the Crown Commercial Service', 
    'bio'               => '<p>He first joined CCS in May 2016 and led the Procurement Operations directorate. He served as interim CEO from July 2018 until his permanent appointment, which followed an external competition, in December that year.</p><p>Simon is an experienced Chief Executive Officer and has extensive experience of strategic planning, customer service, driving performance improvement and achieving results. He first joined the Civil Service in 2008, taking up the position of Chief Executive Officer for the Driver and Vehicle Licensing Agency, where he remained in post until 2013.</p><p>Simon was Health Director at the Department for Work and Pensions, one of the UK’s largest public service departments, from 2013 - 2016. In this role he was responsible for the provision of all health and disability assessments services within the UK.</p><p>This role followed a successful career in the private sector spanning more than 25 years, most recently at Virgin Media, firstly as Managing Director for Wales and West, and subsequently as the UK SME Business Director.</p>', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '2', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/MalcolmHarrison.png', 
        'alt' => '' 
    ), 
    'name'              => 'Malcolm Harrison', 
    'company'           => '', 
    'position'          => 'Group CEO, CIPS', 
    'bio'               => '<p>Malcolm joined Mars in 1982 as a management trainee after graduating from Cambridge University with a degree in Chemical Engineering. He has a broad international experience in consumer facing companies in both general management and global functional roles, particularly Procurement. In his early career he worked in Production, Sales, Procurement, Supply Chain, and HR with Mars Confectionery, Pedigree Petfoods and Bass.  In 1993, he was appointed Purchasing Director of Bass Brewers where he established their first Purchasing function. From 1996 he was CEO of Bass Brewer’s UK Midlands free trade business before joining the Board of Britvic Soft Drinks in 1999 as Operations Director.</p><p>From 2000 he created and led global Procurement for Interbrew, participating in the acquisition and integration of several brewers – in Europe, Asia and Latin America - including AmBev in 2004; he was subsequently Chief Procurement Officer of InBev. In 2006 Malcolm joined Nestlé SA in Switzerland as Chief Procurement Officer responsible for the largest FMCG procurement spend in the world (CHF 60 Bn across 80 countries), heavily focused on global agricultural commodities; he brought a real focus to building the capability of the function and led a step-change in involvement of the procurement function with the Nestlé businesses. He joined the Executive Committee of Rexam plc, the FTSE 100 consumer packaging company, in 2010 as CEO of the global Plastic Packaging division (turnover of £ 1.3 Bn with 45 factories located in 9 countries) serving customers in Healthcare, Cosmetics and FMCG businesses. After leading its successful disposal, he worked in beverage cans during 2013 and his responsibilities included global IT, Environment Health & Safety and Global Procurement where managing the exposure to volatile Plastics and Metal prices was a key responsibility.</p><p>In 2015, Malcolm joined the Cabinet Office of Her Majesty’s Government where he held the role of Chief Executive Officer of the Crown Commercial Service, overseeing an annual spend of more than £13 Bn on common goods and services across the UK public sector. He also had responsibility for UK Public Sector Procurement Policy and for supporting the UK Government’s initiatives with SMEs.</p><p>In July 2018 he joined The Chartered Institute of Procurement & Supply (CIPS) as Group CEO.</p><p>Malcolm lives in the UK and has previously spent in total more than 12 years living in West Africa, France, Belgium and Switzerland as part of his professional career.</p>', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '3', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/MarkPalmer.png', 
        'alt' => '' 
    ), 
    'name'              => 'Mark Palmer', 
    'company'           => '', 
    'position'          => 'Google Cloud, Head of Public Sector, EMEA', 
    'bio'               => '', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '4', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/gillianAskew.jpg', 
        'alt' => '' 
    ), 
    'name'              => 'Gillian Askew', 
    'company'           => '', 
    'position'          => 'Director at All Things Procurement', 
    'bio'               => '<p>Gillian is an experienced procurement professional with a career spanning over 25 years in both private and public sector. Gillian currently runs All Things Procurement, a social enterprise focussing on increasing public sector business to SME\'s and optimising the delivery of social value through procurement.</p><p>Gillian is a Fellow of the Chartered Institute of Procurement and Supply and a member of the Institute of Leadership and Management, as well as a member of FSB and Social Value UK. She is also an active committee member of the CIPS South Yorkshire branch along with a number of other volunteering activities.</p><p>Gillian first joined public sector procurement in 2013 as Head of Procurement for Nottingham Trent University, she was also a non-executive director of NEUPC during her tenure there. She then joined YPO in 2016 as Head of Procurement Services responsible for over £600m public sector spend under management. Prior to this Gillian worked as Supply Chain Director within Babcock International Group leading global procurement and supply chain teams in asset infrastructure. In addition, Gillian has extensive experience as a small business owner including business acquisition and start up.</p>', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '5', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/bencarpenter.jpg', 
        'alt' => '' 
    ), 
    'name'              => 'Ben Carpenter', 
    'company'           => '', 
    'position'          => 'Chief executive of Social Value UK and Social Value International leading a global network of practitioners to develop principles and standards for social value accounting', 
    'bio'               => '<p>Ben Carpenter is the chief executive of Social Value UK and Social Value International leading a global network of practitioners to develop principles and standards for social value accounting.</p><p>Ben is part of the National Social Value Taskforce and the British Standards Industry committee for Social Value. Prior to becoming CEO, Ben was Social Value UK’s Operations Manager developing the international membership, the assurance and accreditation services and led on projects with the OECD, World Economic Forum, Impact Management Project and World Business Council for Sustainable Development. Ben’s background in homelessness, social housing and the built environment, affords him experience that spans sectors and an in-depth knowledge of Social Value accounting.</p>', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '6', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/chrisball.jpg', 
        'alt' => '' 
    ), 
    'name'              => 'Chris Ball', 
    'company'           => '', 
    'position'          => 'Research Associate Newcastle University Business School, Honorary Expert Associate for the National Innovation Centre on Ageing and Director of the Centre for Research into the Older Workforce', 
    'bio'               => '<p>Expert on all aspects of policy and practice regarding the ageing workforce. Passionate about the value of older workers, management of an age diverse workforce, development of the "silver economy" and demographic change, including international dimensions on all these. Speaker, commentator and writer.</p><p>Publications include "Managing the Ageing Workforce: An Introductory Guide for HR Professionals", (TAEN, 2007), research reports for Newcastle University and CROW, as well as academic book chapters and many blogs and articles.</p><p>Specialties: Learning, health and well-being, age management, labour market, demographic changes, employee relations.</p>', 
    'url'               => '', 
);

$data['speakers'][] = array(
    'speaker_id'    => '7', 
    'image'              => array(
        'url' => getenv('px_url') . 'static/img/KevinOMalley.jpg', 
        'alt' => '' 
    ), 
    'name'              => 'Kevin O’Malley', 
    'company'           => '', 
    'position'          => 'Innovation Lead/ SBRI Lead, Clean Growth &amp; Infrastructure, Innovate UK', 
    'bio'               => '<p>Kevin is Innovation Lead/ SBRI Lead in the Clean Growth and Infrastructure Directorate at Innovate UK and is responsible for managing internal and external relationships, technology strategy and delivery with partners across central and local government, with the aim of boosting procurement-led innovation.</p><p>Kevin joined Innovate UK in 2018. Prior to coming to Innovate he was City Innovation Team Manager at Bristol City Council, leading the delivery of the Authorities award winning Smart City programme. He has been responsible for writing dozens of successful funding bids, including REPLICATE, Bristol’s £25m H2020 Smart Cities and Communities Lighthouse Project</p>', 
    'url'               => '', 
);

render(null, $data);
