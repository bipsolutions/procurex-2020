<?php
include '../twig.php';

#################### Public Sector Solutions Start ####################

$data['agenda_items']['public_sector'][] = array(
    'agenda_item_id'    => '1', 
    'title'             => 'Welcome Address', 
    'time_range'        => '10:20 - 10:25', 
    'speaker_name'      => 'David Smith', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Former Commercial Director, DWP & Lead Judge, UK National GO Awards 2020/2021', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['public_sector'][] = array(
    'agenda_item_id'    => '2', 
    'title'             => 'Opening Keynote Address', 
    'time_range'        => '10:25 - 10:45', 
    'speaker_name'      => 'Simon Tse', 
    'speaker_company'   => '', 
    'speaker_position'  => 'CEO, Crown Commercial Service', 
    'speaker_bio'       => '<p>Simon Tse was appointed Chief Executive of the Crown Commercial Service in 2018. He first joined CCS in May 2016 and led the Procurement Operations directorate. He served as interim CEO from July 2018 until his permanent appointment, which followed an external competition, in December that year.</p><p>Simon is an experienced Chief Executive Officer and has extensive experience of strategic planning, customer service, driving performance improvement and achieving results. He first joined the Civil Service in 2008, taking up the position of Chief Executive Officer for the Driver and Vehicle Licensing Agency, where he remained in post until 2013.</p><p>Simon was Health Director at the Department for Work and Pensions, one of the UK’s largest public service departments, from 2013 - 2016. In this role he was responsible for the provision of all health and disability assessments services within the UK.</p><p>This role followed a successful career in the private sector spanning more than 25 years, most recently at Virgin Media, firstly as Managing Director for Wales and West, and subsequently as the UK SME Business Director.</p>', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/simonTse.jpg', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['public_sector'][] = array(
    'agenda_item_id'    => '3', 
    'title'             => 'Procurement in a Post-Brexit World', 
    'time_range'        => '10:45 - 11:25', 
    'speaker_name'      => '', 
    'speaker_company'   => '', 
    'speaker_position'  => '<strong>Chairman:</strong> <br><strong>Grahame Steed</strong>, Content, Research and Communications Director, BiP Solutions <br><br><strong>Panel Members:</strong> <br><strong>Tina Holland</strong>, Improvement Manager, Local Government Association<br><strong>Michelle van Troop</strong>, Associate Director of Procurement & Contracting, NHS Leeds Clinical Commissioning Group', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => '<p>With UK government public procurement spend totalling around £284 bn per year, public sector buyers account for around 13% of UK GDP. It’s therefore essential to consider the potential impact of Brexit on various possible scenarios, such as a ‘no-deal Brexit’, on public procurement – as well as highlighting potential areas of opportunity.</p><p>Capturing the opinions of procurement leaders allows for developing a real picture of the level of knowledge and aspirations of the public sector, in addition to what is already known about the legal position of procurement in relation to Brexit.</p> <p>This panel debate will see buyers and suppliers discussing the following:</p><ul><li>How have attitudes towards No Deal Brexit and preparation for it changed since the report was published?</li><li>What are their biggest concerns?</li><li>Now that we are leaving, what else will they be doing to prepare for this eventuality?</li><li>How much of an issue is it?</li><li>Stats about how much public sector business is facilitated with companies outside of the UK, and the ability/capacity of UK suppliers to take up the slack if these businesses no longer support the UK public sector.</li></ul>', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['public_sector'][] = array(
    'agenda_item_id'    => '4', 
    'title'             => 'Applying Commercial Strategy in Public Sector', 
    'time_range'        => '11:25 - 11:45', 
    'speaker_name'      => 'Mark Palmer', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Head of Public Sector & Life Sciences, UK & Ireland at Google', 
    'speaker_bio'       => 'Profile coming soon', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/MarkPalmer.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['public_sector'][] = array(
    'agenda_item_id'    => '5', 
    'title'             => 'Artificial Intelligence Supporting Public Sector', 
    'time_range'        => '11:45 - 12:05', 
    'speaker_name'      => 'Eddie Dowse', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Senior Account Executive (Mid-Market / Commercial) at Salesforce', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

#################### Public Sector Solutions End ####################

#################### Supply Chain & Risk Management Start ####################

$data['agenda_items']['supply_chain'][] = array(
    'agenda_item_id'    => '1', 
    'title'             => 'Today’s Challenges for Professionals in Procurement & Supply', 
    'time_range'        => '11.00 - 11.20 ', 
    'speaker_name'      => 'Malcolm Harrison', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Group CEO, Chartered Institute of Procurement and Supply (CIPS)', 
    'speaker_bio'       => '<p>Malcolm joined Mars in 1982 as a management trainee after graduating from Cambridge University with a degree in Chemical Engineering. He has a broad international experience in consumer facing companies in both general management and global functional roles, particularly Procurement. In his early career he worked in Production, Sales, Procurement, Supply Chain, and HR with Mars Confectionery, Pedigree Petfoods and Bass.  In 1993, he was appointed Purchasing Director of Bass Brewers where he established their first Purchasing function. From 1996 he was CEO of Bass Brewer’s UK Midlands free trade business before joining the Board of Britvic Soft Drinks in 1999 as Operations Director.</p><p>From 2000 he created and led global Procurement for Interbrew, participating in the acquisition and integration of several brewers – in Europe, Asia and Latin America - including AmBev in 2004; he was subsequently Chief Procurement Officer of InBev. In 2006 Malcolm joined Nestlé SA in Switzerland as Chief Procurement Officer responsible for the largest FMCG procurement spend in the world (CHF 60 Bn across 80 countries), heavily focused on global agricultural commodities; he brought a real focus to building the capability of the function and led a step-change in involvement of the procurement function with the Nestlé businesses. He joined the Executive Committee of Rexam plc, the FTSE 100 consumer packaging company, in 2010 as CEO of the global Plastic Packaging division (turnover of £ 1.3 Bn with 45 factories located in 9 countries) serving customers in Healthcare, Cosmetics and FMCG businesses. After leading its successful disposal, he worked in beverage cans during 2013 and his responsibilities included global IT, Environment Health & Safety and Global Procurement where managing the exposure to volatile Plastics and Metal prices was a key responsibility.</p><p>In 2015, Malcolm joined the Cabinet Office of Her Majesty’s Government where he held the role of Chief Executive Officer of the Crown Commercial Service, overseeing an annual spend of more than £13 Bn on common goods and services across the UK public sector. He also had responsibility for UK Public Sector Procurement Policy and for supporting the UK Government’s initiatives with SMEs.</p><p>In July 2018 he joined The Chartered Institute of Procurement & Supply (CIPS) as Group CEO.</p><p>Malcolm lives in the UK and has previously spent in total more than 12 years living in West Africa, France, Belgium and Switzerland as part of his professional career.</p>', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/MalcolmHarrison.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['supply_chain'][] = array(
    'agenda_item_id'    => '2', 
    'title'             => 'Understanding Supply Chains Through Data', 
    'time_range'        => '11.20 - 11.40', 
    'speaker_name'      => 'Grahame Steed', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Content, Research and Communications Director, BiP Solutions', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['supply_chain'][] = array(
    'agenda_item_id'    => '3', 
    'title'             => 'Risk Management Linked to Insurance Procurement ', 
    'time_range'        => '11.40 - 12.00', 
    'speaker_name'      => 'Agnieszka Gajli', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Category Manager - Corporate Services at YPO', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

#################### Supply Chain & Risk Management End ####################

#################### Technology & Digital Start ####################

$data['agenda_items']['technology_digital'][] = array(
    'agenda_item_id'    => '1', 
    'title'             => 'Bringing Innovators and Authorities Together to Tackle Key Urban Challenges: Lessons from our Domestic Missions Pilot', 
    'time_range'        => '12.00 - 12.30', 
    'speaker_name'      => 'Kevin O\'Malley', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Innovate UK', 
    'speaker_bio'       => '<p>Kevin O’Malley, Innovation Lead/ SBRI Lead in the Clean Growth and Infrastructure Directorate, Innovate UK</p><p>Kevin is Innovation Lead/ SBRI Lead in the Clean Growth and Infrastructure Directorate at Innovate UK and is responsible for managing internal and external relationships, technology strategy and delivery with partners across central and local government, with the aim of boosting procurement-led innovation.</p><p>Kevin joined Innovate UK in 2018. Prior to coming to Innovate he was City Innovation Team Manager at Bristol City Council, leading the delivery of the Authorities award winning Smart City programme. He has been responsible for writing dozens of successful funding bids, including REPLICATE, Bristol’s £25m H2020 Smart Cities and Communities Lighthouse Project</p>', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/KevinOMalley.jpg', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['technology_digital'][] = array(
    'agenda_item_id'    => '2', 
    'title'             => 'Effective Management of a Dynamic Purchasing System', 
    'time_range'        => '12.30 - 13.00', 
    'speaker_name'      => 'Grant Campbell', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Business Development Manager, Delta eSourcing', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.jpg', 
        'alt' => '' 
    ), 
);

#################### Technology & Digital End ####################

#################### Social Value Start ####################

$data['agenda_items']['social_value'][] = array(
    'agenda_item_id'    => '1', 
    'title'             => 'Delivering Social & Economic Value for Your Community Through Effective SME Engagement', 
    'time_range'        => '11.00 – 11.30', 
    'speaker_name'      => 'Gillian Askew', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Director, All Things Procurement', 
    'speaker_bio'       => '<p>Gillian is an experienced procurement professional with a career spanning over 25 years in both private and public sector. Gillian currently runs All Things Procurement, a social enterprise focussing on increasing public sector business to SME\'s and optimising the delivery of social value through procurement.</p><p>Gillian is a Fellow of the Chartered Institute of Procurement & Supply and a member of the Institute of Leadership & Management, as well as a member of FSB and Social Value UK. She is also an active committee member of the CIPS South Yorkshire branch along with a number of other volunteering activities</p><p>Gillian first joined public sector procurement in 2013 as Head of Procurement for Nottingham Trent University, she was also a non-executive director of NEUPC during her tenure there. She then joined YPO in 2016 as Head of Procurement Services responsible for over £600m public sector spend under management. Prior to this Gillian worked as Supply Chain Director within Babcock International Group leading global procurement & supply chain teams in asset infrastructure. In addition, Gillian has extensive experience as a small business owner including business acquisition and start up.</p>', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/gillianAskew.jpg', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['social_value'][] = array(
    'agenda_item_id'    => '2', 
    'title'             => 'Get Strategic with Social Value<br>Find Out How to Grow Your Business Through Embedding International Standards for Social Value', 
    'time_range'        => '11.30 – 12.00', 
    'speaker_name'      => 'Ben Carpenter', 
    'speaker_company'   => '', 
    'speaker_position'  => 'CEO, Social Value UK', 
    'speaker_bio'       => '<p>Ben Carpenter is the chief executive of Social Value UK and Social Value International leading a global network of practitioners to develop principles and standards for social value accounting.</p><p>Ben is part of the National Social Value Taskforce and the British Standards Industry committee for Social Value. Prior to becoming CEO, Ben was Social Value UK’s Operations Manager developing the international membership, the assurance and accreditation services and led on projects with the OECD, World Economic Forum, Impact Management Project and World Business Council for Sustainable Development. Ben’s background in homelessness, social housing and the built environment, affords him experience that spans sectors and an in-depth knowledge of Social Value accounting.</p>', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/bencarpenter.jpg', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['social_value'][] = array(
    'agenda_item_id'    => '3', 
    'title'             => 'Supporting Active Ageing in the Procurement Process', 
    'time_range'        => '12.00 - 12.30', 
    'speaker_name'      => 'Chris Ball', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Research Associate Newcastle University Business Schoo', 
    'speaker_bio'       => '<p>Expert on all aspects of policy and practice regarding the ageing workforce. Passionate about the value of older workers, management of an age diverse workforce, development of the "silver economy" and demographic change, including international dimensions on all these. Speaker, commentator and writer.</p><p>Publications include "Managing the Ageing Workforce: An Introductory Guide for HR Professionals", (TAEN, 2007), research reports for Newcastle University and CROW,  as well as academic book chapters and many blogs and articles.</p><p>Specialties: Learning, health and well-being, age management, labour market, demographic changes, employee relations.</p>', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/chrisball.jpg', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['social_value'][] = array(
    'agenda_item_id'    => '4', 
    'title'             => 'Tackling Modern Slavery in Your Supply Chains', 
    'time_range'        => '12.30 - 13.00', 
    'speaker_name'      => 'Emma Scott', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Representation Manager, Chartered Institute of Procurement and Supply (CIPS)', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['social_value'][] = array(
    'agenda_item_id'    => '5', 
    'title'             => 'Using Social Value across Public Sector', 
    'time_range'        => '13.30 - 14.00', 
    'speaker_name'      => 'Guy Battle', 
    'speaker_company'   => '', 
    'speaker_position'  => 'CEO, Social Value Portal', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => '', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

#################### Social Value End ####################

#################### Procurement Skills Start ####################

$data['agenda_items']['procurement_skills'][] = array(
    'agenda_item_id'    => '1', 
    'title'             => 'Writing a Tender Specification', 
    'time_range'        => '10.15 - 10.45', 
    'speaker_name'      => 'Phil Kinnell', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Senior Consultancy and Training Manager', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'The tender specification is the heart of every procurement exercise and it has to be right, first time, every time. There are a number of key aspects to creating a good tender specification and this session looks at a range of issues and considers their impact, from the technical requirement to criteria and weightings and from service levels to sustainability.', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['procurement_skills'][] = array(
    'agenda_item_id'    => '2', 
    'title'             => 'Is Social Value Achievable?', 
    'time_range'        => '11.00 - 11.30', 
    'speaker_name'      => 'Phil Kinnell', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Senior Consultancy and Training Manager', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'As the importance of Social Value in public procurement exercises has increased, so have the myriad terms used to describe it. Our webinar will define Social Value through examples, and will discuss the common challenges faced by procurers who want to deliver social, economic or environmental benefits through procurement exercises in a legally compliant way.', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['procurement_skills'][] = array(
    'agenda_item_id'    => '3', 
    'title'             => 'Impact of Brexit on Public Procurement', 
    'time_range'        => '11.45-12.15', 
    'speaker_name'      => 'Phil Kinnell', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Senior Consultancy and Training Manager', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'While the impact of Brexit on public procurement is likely to be limited, at least in the short term, this session will discuss the practical considerations for public procurers, and will look at the potential impact on legislation and public procurement policy should a ‘no deal’ Brexit occur at any point.', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['procurement_skills'][] = array(
    'agenda_item_id'    => '4', 
    'title'             => 'Evaluation Techniques', 
    'time_range'        => '13.15-13.45', 
    'speaker_name'      => 'Eddie Regan', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Principal Procurement Consultant', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'It is important that the evaluation stage of the procurement process is structured and finalised before the tender specification is issued. Ensuring the right evaluators are in place and that they have a clear concise understanding of what the authority requires, is also a key element. This session will look at some evaluation issues and offer some tips on ensuring you have a robust process, that delivers the best possible outcome.', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['procurement_skills'][] = array(
    'agenda_item_id'    => '5', 
    'title'             => 'Contract Management', 
    'time_range'        => '13.15 - 13.45', 
    'speaker_name'      => 'Eddie Regan', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Principal Procurement Consultant', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'The European Union (Award of Public Authority Contracts) Regulations 2016, when combined with good contract management processes, offers public authorities the ability to get far better outcomes from their procurement exercises. From managing the service delivery to looking at benchmarking, there are numerous ways in which contract management can help. But, putting the right structure and processes in place is essential. This session looks at a few of the easy wins and identifies areas for consideration', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['procurement_skills'][] = array(
    'agenda_item_id'    => '6', 
    'title'             => 'You did what? Cautionary Tales!', 
    'time_range'        => '14.45 - 15.15', 
    'speaker_name'      => 'Eddie Regan', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Principal Procurement Consultant', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'It’s always less painful to learn from the mistakes of others than to experience them yourself. This session will review a variety of stories and legal judgments and will discuss the most important lessons from them.', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

#################### Procurement Skills End ####################

#################### Bid Support Start ####################

$data['agenda_items']['bid_support'][] = array(
    'agenda_item_id'    => '1', 
    'title'             => 'Reading the Specification', 
    'time_range'        => '09.45-10.15', 
    'speaker_name'      => 'Eddie Regan', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Principal Procurement Consultant', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'As the tender specification is arguably the heart of every procurement exercise, developing a quick and accurate understanding of specifications you encounter will help you to carry out your bid/no bid decision making process. This session will discuss what should be included in specifications and what to look out for.', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['bid_support'][] = array(
    'agenda_item_id'    => '2', 
    'title'             => 'Understanding Frameworks & Dynamic Purchasing Systems', 
    'time_range'        => '10.30-11.00', 
    'speaker_name'      => 'Eddie Regan', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Principal Procurement Consultant', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'The output of a successful public procurement exercise is either the conclusion of a Contract or the establishment of a Framework Agreement or Dynamic Purchasing System.', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['bid_support'][] = array(
    'agenda_item_id'    => '3', 
    'title'             => 'Understanding Added Value', 
    'time_range'        => '11.15-11.45', 
    'speaker_name'      => 'Eddie Regan', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Principal Procurement Consultant', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'How often have you looked at the question about ‘Added Value’ and wondered what exactly the buyer meant? In a perfect world the specification should explain the requirement in detail, however when it doesn’t, it’s often still possible to develop an understanding of what the buyer wants. This session will look at a variety of possible ‘Added Value’ options and explain how you may already be providing exactly what the buyer is seeking, without having realised it.', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['bid_support'][] = array(
    'agenda_item_id'    => '4', 
    'title'             => 'Impact of Brexit on Public Procurement', 
    'time_range'        => '13.00-13.30', 
    'speaker_name'      => 'Phil Kinnell', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Senior Consultancy and Training Manager', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'While the impact of Brexit on public procurement is likely to be limited, at least in the short term, this session will discuss the practical considerations for suppliers, and will look at the potential impact on legislation and public procurement policy should a ‘no deal’ Brexit occur at any point.', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['bid_support'][] = array(
    'agenda_item_id'    => '5', 
    'title'             => 'What is Social Value and is it Deliverable?', 
    'time_range'        => '13.45-14.15', 
    'speaker_name'      => 'Phil Kinnell', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Senior Consultancy and Training Manager', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'As the importance and use of Social Value in public procurement exercises has increased, so have the myriad terms used to describe it. Our webinar will discuss Social Value and its synonyms, using examples, discussing the challenges and suggesting practical steps that will help differentiate your bids from your competitors.', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

$data['agenda_items']['bid_support'][] = array(
    'agenda_item_id'    => '6', 
    'title'             => 'Do\'s and Dont\'s of Tendering', 
    'time_range'        => '14.30-15.00', 
    'speaker_name'      => 'Phil Kinnell', 
    'speaker_company'   => '', 
    'speaker_position'  => 'Senior Consultancy and Training Manager', 
    'speaker_bio'       => '', 
    'speaker_url'       => '', 
    'overview'          => 'Surprisingly, many tenderers make the same mistakes, again and again, when bidding for contracts. From outdated policies, to poorly presented financial information, by way of logistical and capacity problems and poor references, the list of errors is long, but resolving them is relatively easy. This session identifies many of these common errors and offers some easy fixes and some longer term but necessary improvements that many need to make.', 
    'speaker_image'     => array(
        'url' => getenv('px_url') . 'static/img/no_image.png', 
        'alt' => '' 
    ), 
);

#################### Bid Support End ####################

render('themes', $data);
